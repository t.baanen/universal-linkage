\section{Main}

The |Main| module combines all the results up to now into a the algorithm that simulates Kempe's universal linkages.
We define an interactive application that draws configurations and apply it to several linkages including Kempe's linkage for a number of polynomials.

\begin{code}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Main where

import Prelude hiding (Real, subtract)
import Control.Monad.State
import Data.Fixed
import Data.Foldable
import Data.Graph
import qualified Data.Map as Map
import Data.Maybe
import Data.Monoid
import Debug.Trace
import Graphics.Gloss
import Graphics.Gloss.Data.ViewPort
import Graphics.Gloss.Interface.Pure.Game

import Algebra
import Gadget
import Linkage
import Parametrize
import Parser
import Polynomial
import Trigonometry
import Reals
import Util
\end{code}

We represent an annulus using its center point, its inner radius and outer radius.
\begin{code}
type Annulus = (Real2, Real, Real)
\end{code}

Compute the configuration of a linkage drawing the given polynomial within an annulus in $\R^2$, given the linkage's output point.
Implements the construction of \ref{st:main}.
\begin{code}
computeConfig :: Polynomial Real Var2 -> Annulus -> Real2 -> Configuration Real Real2
computeConfig f (midpoint, inner, outer) p
    = computeConfig' (translateXY f $ translation) averageRadius thickness (p `add` translation)
    |> translateConfig midpoint where
        averageRadius = (inner + outer) / 2
        thickness = (outer - inner) / 2
        translation = negate midpoint
\end{code}

Performs the construction given an annulus with center point $(0, 0)$.
The annulus is represented by an average radius and thickness.
Implements the construction of \ref{st:main}, see there for more details.
\begin{code}
computeConfig' :: Polynomial Real Var2 -> Real -> Real -> Real2 -> Configuration Real Real2
computeConfig' f r1 r2 p = adjacencyToFixedLinkage assembled where
    fAsTrigonometry :: Polynomial Real TrigonometricVar2
    fAsTrigonometry = substitute (pointToRadial r1 r2) . expand $ f
\end{code}
Maps indices $r$ and $s$ to $f_{r,s}$ and $\gamma_{r,s}$.
\begin{code}
    fGammas
        = eliminateSines
        . simplifyTrigCoeff
        . expandedTrigCoeff
        . expand
        . trigProdToSum
        . expand
        $ fAsTrigonometry
\end{code}
All the indices $r$ and $s$.
\begin{code}
    rs = Map.foldWithKey (const . (:)) [] fGammas
    assembled = runNames $ do
        origin <- getName
        m1 <- getName
        n1 <- getName
        outputVertex <- getName
        let inputParallelogram = Parallelogram origin m1 n1 outputVertex r1 r2

        parallelogram <- calcParallelogramP1P4 inputParallelogram (0, 0) p
        let pM1 = parallelogram `nodePosition` m1
        let pN1 = parallelogram `nodePosition` n1
        let alpha = atan2 (snd pM1) (fst pM1)
        let beta = atan2 (snd pN1) (fst pN1)
        xAxis <- getName

        angles <- sequence [reversor alpha beta m1 pM1 n1 pN1 origin xAxis r s | (r, s) <- rs]
\end{code}
Now we sum all the cosines using translators.
Place a point far enough away so that the sum of all the cosines does not exceed the distance between the point and the origin.
\begin{code}
        let (a, b) = (sum [abs f | (f, g) <- (fGammas Map.!) <$> rs], 0)

        w0 <- getName
        let translationBase = Map.fromList [(w0, ((a, b), Map.empty))]
        (output, sum) <- foldr (sumDistances origin a) (pure (w0, translationBase)) angles
        let pOutput = sum `nodePosition` output
\end{code}

Place a Peaucellier linkage that constrains the output vertex to lie within the vertical line $x = a$.
\begin{code}
        constrainer <- peaucellier a b origin output pOutput (a / 10)

        return $
            sum `glueAPL`
            constrainer `glueAPL`
            foldr (glueAPL . snd) parallelogram angles
\end{code}

The Peaucellier linkage can be scaled arbitrarily until it is large enough. Because of our construction, it needs to be able to reach $(a, \pm a)$. For ease of defining, the basic Peaucellier linkage can reach $(10, 10)$.
\begin{code}
    peaucellier a b origin output pOutput scale = do
        let offset = scale * 10
        let innerRadius = scale
        let rhombusSide = scale * 5
        let triangleSide = scale * sqrt 45

        o <- getName
        inverted <- getName
        x <- getName
        let pO = (a - offset, b)
        let pX = (a - offset + innerRadius, b)
        calcPeaucellier
            (Peaucellier o output inverted x innerRadius rhombusSide triangleSide)
            pO pOutput pX
\end{code}

%format angleAlpha = "r \alpha"
%format angleBeta = "s \beta"
Use reversors to multiply angles |alpha| and |beta| by $r$ and $s$ respectively.
\begin{code}
    reversor :: Real -> Real -> Node -> Real2 -> Node -> Real2 -> Node -> Node -> Real -> Real -> Naming (Node, APL)
    reversor alpha beta m1 pM1 n1 pN1 origin xAxis r s = do
        let (f, g) = fGammas Map.! (r, s)

        let angleAlpha = alpha * r
        let angleBeta = beta * s
        multipliedAlpha <- getName
        multipliedBeta <- getName
        let pAlpha = (f * cos angleAlpha, f * sin angleAlpha)
        let pBeta = (f * cos angleBeta, f * sin angleBeta)

        (_, multAlpha) <- calcMultiplicator
            (Multiplicator (round r) origin xAxis m1 multipliedAlpha 1 (norm pM1) f)
            0 angleAlpha (0, 0) (1, 0) pM1 pAlpha
        (_, multBeta) <- calcMultiplicator
            (Multiplicator (round s) origin xAxis n1 multipliedBeta 1 (norm pN1) f)
            0 angleBeta (0, 0) (1, 0) pN1 pBeta

        gamma <- getName
        let pGamma = (f * cos g, f * sin g)

        alphaPlusBeta <- getName
        let pAlphaPlusBeta = (f * cos (angleAlpha + angleBeta), f * sin (angleAlpha + angleBeta))
        alphaPlusBetaPlusGamma <- getName
        let pAlphaPlusBetaPlusGamma = (f * cos (angleAlpha + angleBeta + g), f * sin (angleAlpha + angleBeta + g))

        additorAB <- calcAdditor
            (Additor origin xAxis multipliedAlpha multipliedBeta alphaPlusBeta 1 f f f)
            (0, 0) (1, 0) pAlpha pBeta pAlphaPlusBeta
        additorABC <- calcAdditor
            (Additor origin xAxis alphaPlusBeta gamma alphaPlusBetaPlusGamma 1 f f f)
            (0, 0) (1, 0) pAlphaPlusBeta pGamma pAlphaPlusBetaPlusGamma

        return 
            ( alphaPlusBetaPlusGamma
            , multAlpha `glueAPL` multBeta `glueAPL` additorAB `glueAPL` additorABC)
\end{code}

Use translators to add the position of the given node to the result.
\begin{code}
    sumDistances :: Node -> Real -> (Node, APL) -> Naming (Node, APL) -> Naming (Node, APL)
    sumDistances origin scale (a, lA) nlb = do
        (b, lB) <- nlb
        sum <- getName
        let pA = lA `nodePosition` a
        let pB = lB `nodePosition` b
        lSum <- calcTranslator (Translator origin a b sum (norm pA) scale) (0, 0) pA pB
        return (sum, lA `glueAPL` lB `glueAPL` lSum)
\end{code}

\subsection{Drawing a linkage}

We define a number of functions that represent a configuration of a linkage in $\R^2$.
\begin{code}
vertexPicture :: Picture
vertexPicture = circleSolid 0.02 |> color red <>
                circle 0.02 |> color black

drawVertices :: Graph -> Map.Map Vertex Real2 -> Picture
drawVertices graph positions = foldMap (drawVertex . getPosition) $ vertices graph
    where drawVertex :: Real2 -> Picture
          drawVertex (x, y) = vertexPicture |> translate x y
          getPosition :: Vertex -> Real2
          getPosition = (positions Map.!)
drawEdges :: Graph -> Map.Map (Vertex, Vertex) Real -> Map.Map Vertex Real2 -> Picture
drawEdges graph lengths positions = foldMap (drawEdge . getPositions) $ edges graph
    where drawEdge :: (Real, Real2, Real2) -> Picture
          drawEdge (length, p, q) = line [p, q] <> lengthLabel length (p, q)
          getPositions :: (Vertex, Vertex) -> (Real, Real2, Real2)
          getPositions edge@(p, q) = (lengths Map.! edge, positions Map.! p, positions Map.! q)
          lengthLabel :: Real -> (Real2, Real2) -> Picture
          lengthLabel length (p, q) = text (show length)
                                    |> scale 0.001 0.001
                                    |> uncurry translate (average p q)

drawLinkage :: Configuration Real Real2 -> Picture
drawLinkage (FixedLinkage (Linkage graph edgeLengths) positions) =
    drawVertices graph positions <>
    drawEdges graph edgeLengths positions
\end{code}

\subsection{Zooming and animating}

To allow the user to zoom in while also getting the mouse position, we have to construct our own viewport state similar to the one used in Gloss.
\begin{code}

data DisplayWorld = DisplayWorld
    { worldDrawPoint :: Real2
    , worldViewPort :: ViewPort
    , worldTime :: Float
    , screenSize :: (Float, Float)
    }

handleEvents :: Event -> DisplayWorld -> DisplayWorld
handleEvents (EventMotion pos') world
    = world { worldDrawPoint = invertViewPort port pos' } where
        port = worldViewPort world
handleEvents (EventKey (MouseButton WheelUp) Down _ _) world
    = world { worldViewPort = newPort } where
        newPort = (worldViewPort world) { viewPortScale = viewPortScale (worldViewPort world) * 2 }
handleEvents (EventKey (MouseButton WheelDown) Down _ _) world
    = world { worldViewPort = newPort } where
        newPort = (worldViewPort world) { viewPortScale = viewPortScale (worldViewPort world) / 2 }
handleEvents _ world = world
\end{code}

A simple function to go to a new timestep by adding it to the elapsed time.
\begin{code}
incrementTime :: Float -> DisplayWorld -> DisplayWorld
incrementTime timeStep world = world { worldTime = worldTime world + timeStep }
\end{code}

\subsection{Linkage subjects}
We can animate a linkage given the curve and the annulus to draw it in. Every point in the interpolation should lie within the annulus.
\begin{code}
data LinkageSubject = Subject InterpolatedCurve Annulus
\end{code}

Also handle the parametrized curves here.
\begin{code}
parametrizedSubject :: ParametrizedCurve -> Real -> Real -> Annulus -> LinkageSubject
parametrizedSubject curve start end = Subject $ interpolateCurve curve start end
\end{code}

\begin{code}
subjectOutputForTime :: LinkageSubject -> Float -> Real2
subjectOutputForTime (Subject (Interpolated polynomial points) annulus) time
    = point parameter where
        point = lerpCurve points
        pointCount = length points
        parameter = (time * fromIntegral pointCount / 20) `mod'` (fromIntegral pointCount - 1)

subjectAtTimestep :: LinkageSubject -> Float -> Configuration Real Real2
subjectAtTimestep subject@(Subject (Interpolated polynomial points) annulus) time
    = computeConfig polynomial annulus $ subjectOutputForTime subject time
\end{code}

\subsection{Drawing}
To show different kinds of linkages, we need to abstract over the drawing of it.
This class should be separately implemented by those kinds.
\begin{code}
class Drawable d where
\end{code}
Text shown on the screen above everything else.
\begin{code}
    description :: d -> String
\end{code}
Similar to the description, but can be updated depending on the user input.
\begin{code}
    interactiveDescription :: d -> Float -> (Float, Float) -> String
    interactiveDescription _ _ _ = ""
\end{code}
Pictures that can be zoomed into using the scroll wheel.
The interactive picture has a time and mouse location as input values.
\begin{code}
    drawBackground :: d -> Picture
    drawInteractive :: d -> Float -> (Float, Float) -> Picture
\end{code}

We provide these default implementations of the |Drawable| class for mouse-input and time-input linkages.
\begin{code}
instance Drawable LinkageSubject where
    description (Subject (Interpolated p _) _) = ppPolynomial p
    interactiveDescription subject time _ =
        show edges ++ " edges, " ++ show vertices ++ " vertices" where
            (edges, vertices) = countParts $ subjectAtTimestep subject time
    drawBackground (Subject (Interpolated _ points) _) = color blue . line $ points
    drawInteractive subject time _ = drawLinkage linkage <>
        circle 0.05 |> color blue |> uncurry translate (subjectOutputForTime subject time) where
            linkage = subjectAtTimestep subject time

instance Drawable (String, Real2 -> FixedLinkage Real Real2) where
    description = fst
    interactiveDescription (_, subject) _ mouse =
        show edges ++ " edges, " ++ show vertices ++ " vertices" where
            (edges, vertices) = countParts $ subject mouse
    drawBackground _ = blank
    drawInteractive (_, conf) _ point = drawLinkage . conf $ point
\end{code}

Rendering something is simply a question of feeding the parameters to a |Drawable| instance.
\begin{code}
render :: (Drawable d) => d -> DisplayWorld -> Picture
render drawable world
    = drawDescription (description drawable)
    <> drawInteractiveDescription interactiveDesc
    <> applyViewPortToPicture (worldViewPort world)
        ( drawBackground drawable
        <> drawInteractive drawable (worldTime world) (worldDrawPoint world)
        ) where
            top, left :: Float
            left = -0.5 * (fst . screenSize) world
            top = 0.45 * (snd . screenSize) world
            drawDescription desc = text desc |> scale 0.1 0.1 |> translate left top
            drawInteractiveDescription desc = text desc |> scale 0.1 0.1 |> translate left (top + 10)
            interactiveDesc = (interactiveDescription drawable (worldTime world) (worldDrawPoint world))
\end{code}

\subsection{Interesting subjects}
We include a couple of subjects for easy demonstration.
\begin{code}
parabola :: LinkageSubject
parabola = parametrizedSubject
    (Parametrized
        (runParsePolynomial "x^2 - y")
        (\x -> (x, x**2))
    )
    (-2) 2
    ((0, 2), 1, 4)
parabolaImplicit :: LinkageSubject
parabolaImplicit = Subject
    (interpolateImplicit (-4, -4) (4, 4) (runParsePolynomial "x^2 - y"))
    ((0, 2), 1, 8)
node :: LinkageSubject
node = Subject
    (interpolateImplicit (-4, -4) (4, 4) (runParsePolynomial "x^2 - y^2"))
    ((0, 2), 1, 8)
cusp :: LinkageSubject
cusp = Subject
    (interpolateImplicit (-4, -4) (4, 4) (runParsePolynomial "x^2 - y^3"))
    ((0, 2), 1, 8)
tacnode :: LinkageSubject
tacnode = Subject
    (interpolateImplicit (-4, -4) (4, 4) (runParsePolynomial "x^2 - y^4"))
    ((0, 2), 1, 8)
ramphoid :: LinkageSubject
ramphoid = Subject
    (interpolateImplicit (-4, -4) (4, 4) (runParsePolynomial "x^2 - y^5"))
    ((0, 2), 1, 8)

userInput :: LinkageSubject
userInput = Subject
    (interpolateImplicit (-4, -4) (4, 4) (runParsePolynomial "y"))
    ((-5, 0), 1, 10)
\end{code}

Similarly, we include some components of the final linkage for the user to interact with.
These all draw some input point at the place where the user puts their mouse.

\begin{code}
reversor :: Real2 -> FixedLinkage Real Real2
reversor p = adjacencyToFixedLinkage . runNames $
    calcReversor (Reversor "O" "X" "Y" "Z" 1 1 1) (0, 0) angle (1, 0) minAngle where
        angle = normalize p
        minAngle = (fst angle, - snd angle)

parallelogram :: Real2 -> FixedLinkage Real Real2
parallelogram p = adjacencyToFixedLinkage . runNames $
    calcParallelogramP1P4 (Parallelogram "O" "X" "M" "N" 1 2) (0, 0) p

contraparallelogram :: Real2 -> FixedLinkage Real Real2
contraparallelogram p = adjacencyToFixedLinkage . runNames $
    calcContraparallelogramP2P4 (Contraparallelogram "O" "X" "M" "N" 1 2) (0, 0) p
\end{code}

These are all collected in a single list for easy switching between them.
\begin{code}
renders :: [DisplayWorld -> Picture]
renders =
    [ render userInput
    , render parabola
    , render parabolaImplicit
    , render node
    , render cusp
    , render tacnode
    , render ramphoid
    , render ("Reversor", reversor)
    , render ("Parallelogram", parallelogram)
    , render ("Contraparallelogram", contraparallelogram)
    ]
\end{code}

\subsection{Drawing switching}
To allow the user to switch between drawables, we introduce the |Switch| data type that represents a drawable along with a list of other drawables.
By pressing the space key, the user can switch to the next item in the list.

\begin{code}
data Switch world = Switch {
    displayWorld :: world, renderer :: world -> Picture,
    otherRenderers :: [world -> Picture],
    defaultEvents :: Event -> world -> world,
    defaultTime :: Float -> world -> world
}

switchRender :: Switch world -> Picture
switchRender switch = renderer switch . displayWorld $ switch

switchEvents :: Event -> Switch world -> Switch world
switchEvents (EventKey (SpecialKey KeySpace) Up _ _) switch
    = switch
        { renderer = head . otherRenderers $ switch
        , otherRenderers = (tail . otherRenderers $ switch) ++ [renderer switch]
        }
switchEvents event switch
    = switch
        { displayWorld = defaultEvents switch event (displayWorld switch)
        }

switchTime :: Float -> Switch world -> Switch world
switchTime time switch = switch { displayWorld = defaultTime switch time (displayWorld switch) }

playSwitch ::
    (
        (Switch world -> Picture) ->
        (Event -> Switch world -> Switch world) ->
        (Float -> Switch world -> Switch world) ->
        a
    ) -> a
playSwitch play = play switchRender switchEvents switchTime
\end{code}

Display the linkage on the screen.
\begin{code}
main :: IO ()
main = playSwitch $ play window white 30 switch where
    windowSize :: Num a => (a, a)
    windowSize = (1024, 768)
    window = InWindow "LinkageWindow" windowSize (0, 0)
    viewport :: ViewPort
    viewport = ViewPort (0, 0) 0 20
    world :: DisplayWorld
    world = DisplayWorld (9, 4) viewport 0 windowSize
    switch :: Switch DisplayWorld
    switch = Switch world (head renders) (tail renders) handleEvents incrementTime
\end{code}
