\section{Trigonometry} \label{lhs:Trigonometry}

An important step of Kempe's universality proof revolves around rewriting trigonometric polynomials.
The rewriting functions used are defined in this package, along with utility functions.

\begin{code}

module Trigonometry where

import Prelude hiding (Real, subtract)
import qualified Data.Map as Map

import Algebra
import Polynomial
import Reals
\end{code}

\subsection{Utility}

While this data type is basically equal to |Var2|, it is useful to explicitly note the change in variables when substituting cartesian coordinates with a sum of two polar coordinates.
\begin{code}
data TrigonometricVar2 = Alpha | Beta deriving (Eq, Show)
\end{code}

Sine and cosine functions that take and return arbitrary polynomial parts.
\begin{code}
psin, pcos :: (Ring k, PolynomialPart p, PolynomialPart q) => p k v -> q k v
psin = fromFactor . Sin . toPolynomial
pcos = fromFactor . Cos . toPolynomial
\end{code}

Substitute polar coordinates for cartesian coordinates.
\begin{code}
pointToRadial r1 r2 XX = r1 :*: pcos (Var Alpha) :+: r2 :*: pcos (Var Beta) :+: PEmpty
pointToRadial r1 r2 YY = r1 :*: psin (Var Alpha) :+: r2 :*: psin (Var Beta) :+: PEmpty
\end{code}

\subsection{Product to sum formulae}

Repeatedly apply product to sum formulae to eliminate all products from a trigonometric polynomial.
Replaces expressions like $\cos A \cos B$ with expressions like $\frac{\cos (A+B) + \cos (A-B)}{2}$.

\begin{code}
trigProdToSum :: (Eq v) => Polynomial Real v -> Polynomial Real v
trigProdToSum = until hasNoProducts (expand . trig') where
    trig' = foldPolynomial ((Var, Cos, Sin, Brackets), (IEmpty, (:.:)), coeff, (PEmpty, (:+:)))
    coeff' k = ((k * 0.5) `coeff`)
    coeff :: Real -> PIndeterminates Real v -> PTerm Real v
    k `coeff` (Cos a :.: Cos b :.: p) = coeff' k $ coscos a b :.: p
    k `coeff` (Cos a :.: Sin b :.: p) = coeff' k $ cossin a b :.: p
    k `coeff` (Sin b :.: Cos a :.: p) = coeff' k $ cossin a b :.: p
    k `coeff` (Sin a :.: Sin b :.: p) = coeff' k $ sinsin a b :.: p
    k `coeff` f = k :*: f

    coscos, cossin, sinsin :: (Ring k) => Polynomial k v -> Polynomial k v -> PFactor k v
    coscos a b = toFactor $ pcos (a `add` b) :+: pcos (a `add` invert b)
    cossin a b = toFactor $ psin (a `add` b) :+: invert (psin $ a `add` invert b)
    sinsin a b = toFactor $ pcos (a `add` invert b) :+: invert (pcos (a `add` b))

    hasNoProducts = foldPolynomial
        ( (const True, const True, const True, id)
        , ((True, 0), prod)
        , const fst
        , (True, (&&))
        )
    a `prod` (b, 0) = (a && b, 1)
    a `prod` (b, n) = (False, n+1)
\end{code}

\subsection{Determining coefficients}

While trigonometric polynomials represented by the |Polynomial| data type are easy to substitute and evaluate, we will need to read out coefficients eventually.
Once the |Polynomial| has a sufficiently simple form, we can convert it to a map from its terms to the coefficients.

A (non-unique) representation of all trigonometric functions of a sum of two variables. For example, $\sin (k \alpha + k' \beta)$ is represented by |SinF k k'|.
\begin{code}
data TrigFunction2 k
    = SinF k k
    | CosF k k
    | Constant
    deriving (Show, Eq, Ord)
\end{code}

If a trigonometric polynomial is completely expanded, has no products and its trigonometric functions are sums of variables, determine its coefficients.
Completely expanded means it contains no |Brackets| factors, no |Var| terms outside of |Sin| or |Cos|. Having no products means it contains no indeterminates of the form |Cos p :.: Sin p|.
The function |trigProdToSum| should produce an agreeable form of polynomials.
\begin{code}
expandedTrigCoeff
    :: (Ord k, Ring k)
    => Polynomial k TrigonometricVar2
    -> Map.Map (TrigFunction2 k) k
expandedTrigCoeff PEmpty = Map.empty
expandedTrigCoeff (f :+: g)
    = Map.unionWith add (expandedTrigCoeffT f) (expandedTrigCoeff g) where
    expandedTrigCoeffT (k :*: f) = multiply k <$> expandedTrigCoeffI f
    expandedTrigCoeffI IEmpty = Map.fromList [(Constant, identity)]
    expandedTrigCoeffI (f :.: IEmpty) = expandedTrigCoeffF f

    expandedTrigCoeffF (Var v)
        = error "Untranslated variable, can't determine coefficients"
    expandedTrigCoeffF (Cos f)
        = Map.fromList [(uncurry CosF $ coeff f, identity)]
    expandedTrigCoeffF (Sin f)
        = Map.fromList [(uncurry SinF $ coeff f, identity)]
    expandedTrigCoeffF (Brackets _)
        = error "Can't determine coefficients for whole polynomials!"

    coeff PEmpty = (mempty, mempty)
    coeff (f :+: g) = split32 add (coeffT f) (coeff g)
    coeffT (k :*: f) = split22 (multiply k) (coeffI f)
    --coeffI (IEmpty) = error "Trig function has no variables in its argument!"
    coeffI (f :.: IEmpty) = coeffF f
    coeffF (Var Alpha) = (identity, mempty)
    coeffF (Var Beta) = (mempty, identity)

    uncurry3 :: (a -> b -> c -> d) -> (a, b, c) -> d
    uncurry3 f (a, b, c) = f a b c
    split32 :: (a -> b -> c) -> (a, a) -> (b, b) -> (c, c)
    split32 f (a, b) (x, y) = (f a x, f b y)
    split22 :: (a -> b) -> (a, a) -> (b, b)
    split22 f (a, b) = (f a, f b)
    split33 :: (a -> b -> c) -> (a, a, a) -> (b, b, b) -> (c, c, c)
    split33 f (a, b, c) (x, y, z) = (f a x, f b y, f c z)
    split23 :: (a -> b) -> (a, a, a) -> (b, b, b)
    split23 f (a, b, c) = (f a, f b, f c)
\end{code}

Make the representation in trigonometric functions unique by replacing equivalent terms.
\begin{code}
simplifyTrig :: TrigFunction2 Real -> TrigFunction2 Real
simplifyTrig Constant = CosF mempty mempty
simplifyTrig t = t

simplifyTrigCoeff :: (Group v) => Map.Map (TrigFunction2 Real) v -> Map.Map (TrigFunction2 Real) v
simplifyTrigCoeff = Map.mapKeysWith add simplifyTrig
\end{code}

\subsection{Eliminating sines}

To finish our rewriting of the polynomial, we get rid of any |SinF| term using trigonometric identities.

\begin{code}
eliminateSines :: Map.Map (TrigFunction2 Real) Real -> Map.Map (Real, Real) (Real, Real)
eliminateSines coeffs = Map.fromList [((r, s), fGamma r s) | (r, s) <- getRS coeffs] where
    getRS :: Map.Map (TrigFunction2 Real) Real -> [(Real, Real)]
    getRS = Map.foldWithKey (\k a -> (rs k :)) []
    rs :: (Num x) => TrigFunction2 x -> (x, x)
    rs (CosF a b) = (a, b)
    rs (SinF a b) = (a, b)
    rs Constant = (0, 0)
    a r s = Map.findWithDefault 0 (SinF r s) coeffs
    b r s = Map.findWithDefault 0 (CosF r s) coeffs
    fGamma r s = (f r s, gamma r s)
    -- a sin x + b cos x = sqrt(a^2 + b^2) cos (x + atan2(b, a) - pi/2)
    f r s = sqrt $ a r s ** 2 + b r s ** 2
    gamma r s = atan2 (b r s) (a r s) - pi / 2
\end{code}
