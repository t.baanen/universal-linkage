\section{Integers}

%format Integer = "\Z "

Similarly, we define instances for the built-in types |Integer| and |Int|.

\begin{code}
module Integers where

import Algebra

instance Monoid Integer where
    mempty = 0
    mappend = (+)

instance Group Integer where
    invert n = -n

instance Ring Integer where
    identity = 1
    multiply = (*)
    character = id

instance Monoid Int where
    mempty = 0
    mappend = (+)

instance Group Int where
    invert n = -n

instance Ring Int where
    identity = 1
    multiply = (*)
    character = fromInteger

\end{code}
