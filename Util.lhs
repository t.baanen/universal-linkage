\section{Utilities}

We define a number of small utility functions, which allow us to write our code somewhat more efficiently.

\begin{code}
module Util where

import qualified Data.Map as Map
\end{code}

%format |> = "{\mathbin{|\!\!\!>}}"

Function application with function after argument, handy for describing graphics pipelines.

\begin{code}
(|>) :: a -> (a -> b) -> b
(|>) = flip ($)
\end{code}

%format ... = "{\dots}"
Double function composition.
Alternate definitions include |(...) = (.).(.)|, |(...) = fmap fmap fmap|.

\begin{code}
(...) :: (c -> d) -> (a -> b -> c) -> a -> b -> d
(...) f g x y = f (g x y)
\end{code}

%format choose(n)(k) = "{" n "\choose" k "}"
Give the number of $k$-element subsets of a $n$-element set.
\begin{code}
choose :: Int -> Int -> Int
choose n k = product [n-k+1 .. n] `div` product [1..k]
\end{code}

Concatenate nested maps into a single map with two keys.
\begin{code}
flattenMap :: (Ord k, Ord k') => Map.Map k (Map.Map k' v) -> Map.Map (k, k') v
flattenMap nested = Map.fromList
    [ ((k, k'), v)
    | (k, map') <- Map.toList nested
    , (k', v) <- Map.toList map'
    ]

-- exclusive or operator
xor :: Bool -> Bool -> Bool
xor True True = False
xor True False = True
xor False True = True
xor False False = False

\end{code}
