\section{Linkages} \label{lhs:Linkage}

We define an abstract concept of linkages. This gets specialized in the section about gadgets, \ref{lhs:Gadget}.

\begin{code}
module Linkage where

import Prelude hiding (Real)
import Control.Arrow
import Data.Graph
import qualified Data.Map as Map
import Data.Maybe

import Util
\end{code}

A linkage in the metric space |v|, with bar lengths given by field |k|.
We require that v is specified to make sure it gets passed around
(e.g. when fixing points).

\begin{code}
data Linkage k v = Linkage Graph (Map.Map Edge k)
    deriving (Show)
\end{code}

A linkage with a number of points (namely those in the map) fixed.
\begin{code}
data FixedLinkage k v = FixedLinkage (Linkage k v) (Map.Map Vertex v)
    deriving (Show)
\end{code}

A configuration requires that all the linkage's points are fixed.
\begin{code}
type Configuration = FixedLinkage
\end{code}

We provide a couple of statistics for linkages, mostly for informing the user.
The part counting function returns the number of bars and vertices.
\begin{code}
countParts :: FixedLinkage k v -> (Int, Int)
countParts (FixedLinkage (Linkage graph _) _) = (length . edges $ graph, length . vertices $ graph)
\end{code}

Directly building linkages is somewhat cumbersome, so we define intermediate datatypes...
\begin{code}
type AdjacencyList key length
    = Map.Map key (Map.Map key length)
type AdjacencyPositionList key position length
    = Map.Map key (position, Map.Map key length)
\end{code}

... and code to transform them to a |Linkage|.
\begin{code}
adjacencyToLinkage :: Ord key => AdjacencyList key length -> Linkage length v
adjacencyToLinkage = fst . adjacencyToLinkage'
\end{code}

Generally, we also want to know which vertex is identified with the keys in the original |AdjacencyList|.

%format *** = "{\mathbin{\star^\star\star}}"
\begin{code}
adjacencyToLinkage' :: Ord key => AdjacencyList key length -> (Linkage length v, key -> Vertex)
adjacencyToLinkage' adjacency = (Linkage graph edgeLengths, getVertex) where
    (graph, vertexToNode, keyToVertex) = graphFromEdges
        [ ((), key, Map.keys edges)
        | (key, edges) <- Map.toList adjacency
        ]
    edgeLengths = Map.mapKeys getEdge (flattenMap adjacency)
    getVertex = fromJust . keyToVertex
    getEdge = getVertex *** getVertex
\end{code}

Similarly, we can build a |FixedLinkage| from an |AdjacencyPositionList|.
\begin{code}
adjacencyToFixedLinkage :: Ord key =>
    AdjacencyPositionList key position length -> FixedLinkage length position
adjacencyToFixedLinkage adjacency
    = FixedLinkage linkage fixings where
        (linkage, getVertex) = adjacencyToLinkage' adjacencies
        adjacencies = Map.fromList
            [ (key, edges)
            | (key, (_, edges)) <- Map.toList adjacency
            ]
        fixings = Map.fromList
            [ (getVertex key, position)
            | (key, (position, _)) <- Map.toList adjacency
            ]
\end{code}

Glue two linkages together along any shared vertices.
\begin{code}
glueAPL :: (Eq position, Ord key) =>
    AdjacencyPositionList key position length ->
    AdjacencyPositionList key position length ->
    AdjacencyPositionList key position length
glueAPL = Map.unionWith glueNodes where
    glueNodes (pos, edges) (pos', edges')
        = (pos, edges `Map.union` edges')
\end{code}

Look up a vertex in the |AdjacencyPositionList|.
\begin{code}
nodePosition :: (Ord key) => AdjacencyPositionList key position length -> key -> position
nodePosition linkage node = fst $ linkage Map.! node
\end{code}

Move all the fixed vertices along a given offset.
\begin{code}
translateConfig :: (Monoid v) => v -> FixedLinkage k v -> FixedLinkage k v
translateConfig offset (FixedLinkage nodes config) = FixedLinkage nodes translated where
    translated = Map.map (mappend offset) config
\end{code}
