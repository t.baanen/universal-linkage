\section{QuickCheck} \label{lhs:QuickCheck}

To test the implementation works as expected, we have made a test suite that automatically tries to generate countexamples to the desired properties.

\begin{code}
{-# LANGUAGE TemplateHaskell #-}

module QuickCheck where

import Prelude hiding (Real)
import Control.Applicative
import Control.Monad.Fix
import qualified Data.Map as Map
import Test.QuickCheck

import Main
import Algebra
import Gadget
import Integers
import Parser
import Polynomial
import Reals
import Trigonometry

\end{code}

\subsection{Generating test cases}

The |Arbitrary| typeclass allows the test suite to automatically generate instances of our data structures.

\begin{code}

instance Arbitrary Var1 where
    arbitrary = pure X
instance Arbitrary Var2 where
    arbitrary = elements [XX, YY]
    shrink XX = []
    shrink YY = [XX]
instance Arbitrary TrigonometricVar2 where
    arbitrary = elements [Alpha, Beta]
    shrink Alpha = []
    shrink Beta = [Alpha]
instance (Arbitrary k, Arbitrary v) => Arbitrary (PFactor k v) where
    arbitrary = sized arbitraryF where
        arbitraryF :: (Arbitrary k, Arbitrary v) => Int -> Gen (PFactor k v)
        arbitraryF n | n < 0 = error "arbitrary factor: negative size"
                     | n == 0 = oneof
                         [ Var <$> arbitrary
                         ]
                     | n > 0 = oneof
                         [ Cos <$> resize (n-1) arbitrary
                         , Sin <$> resize (n-1) arbitrary
                         , Brackets <$> resize (n-1) arbitrary
                         ]
    shrink (Var v) = Var <$> shrink v
    shrink (Cos t) = Cos <$> shrink t
    shrink (Sin t) = Sin <$> shrink t
    shrink (Brackets p) = Brackets <$> shrink p
instance (Arbitrary k, Arbitrary v) => Arbitrary (PIndeterminates k v) where
    arbitrary = sized arbitraryI where
        arbitraryI :: (Arbitrary k, Arbitrary v) => Int -> Gen (PIndeterminates k v)
        arbitraryI n | n < 0 = error "arbitrary indeterminates: negative size"
                     | n == 0 = pure IEmpty
                     | n > 0 = do
                        k <- choose (1, n)
                        (:.:) <$> resize (k-1) arbitrary <*> resize (n-k) arbitrary
    shrink IEmpty = []
    shrink (f :.: g) = IEmpty : g : [f' :.: g' | (f', g') <- shrink (f, g)]
instance (Arbitrary k, Arbitrary v) => Arbitrary (PTerm k v) where
    arbitrary = sized arbitraryT where
        arbitraryT :: (Arbitrary k, Arbitrary v) => Int -> Gen (PTerm k v)
        arbitraryT n = (:*:) <$> arbitrary <*> arbitrary
    shrink (k :*: i) = [k' :*: i' | (k', i') <- shrink (k, i)]
instance (Arbitrary k, Arbitrary v) => Arbitrary (Polynomial k v) where
    arbitrary = sized arbitraryP where
        arbitraryP :: (Arbitrary k, Arbitrary v) => Int -> Gen (Polynomial k v)
        arbitraryP n | n < 0 = error "arbitrary polynomial: negative size"
                     | n == 0 = pure PEmpty
                     | n > 0 = do
                        k <- choose (1, n)
                        (:+:) <$> resize (k-1) arbitrary <*> resize (n-k) arbitrary
    shrink PEmpty = []
    shrink (f :+: g) = PEmpty : g : [f' :+: g' | (f', g') <- shrink (f, g)]
\end{code}

\subsection{Equivalence testing}

The usual |(==)| equality operator is not always the right choice. We define two other operators that better represent the desired equivalence between our data types.

\begin{code}
approx :: Float -> Float -> Property
approx x y = counterexample (show x ++ " =/= " ++ show y) smallEnough where
    diff = abs (x - y)
    smallEnough = diff <= 0.01 * abs x && diff <= 0.01 * abs y

counterexampleEquiv
    :: (Ring k, Eq k, Eq v, Show k, Show v)
    => Polynomial k v
    -> Polynomial k v
    -> Property
counterexampleEquiv f g
    = counterexample (show f ++ " =/= " ++ show g) $ expand f === expand g
\end{code}

\subsection{Properties}

Using the above definitions, we can give the properties we expect of our implementation.
Template Haskell allows us to automatically collect all definitions starting with |prop_| to execute these at once.

\begin{code}
prop_eqReflexive
    :: Polynomial Int Var1
    -> Property
prop_eqReflexive f = f === f
prop_eqSymmetric
    :: Polynomial Int Var1
    -> Polynomial Int Var1
    -> Property
prop_eqSymmetric f g = f == g ==> g == f
prop_eqTransitive
    :: Polynomial Int Var1
    -> Polynomial Int Var1
    -> Polynomial Int Var1
    -> Property
prop_eqTransitive f g h = f == g && g == h ==> f == h

prop_expandKeepsEvaluate :: Polynomial Float Var1 -> Float -> Property
prop_expandKeepsEvaluate f x
    | hasTrig f = discard
    | otherwise = evaluateInX f `approx` evaluateInX (expand f) where
        evaluateInX = evaluate (const x)

prop_expandIdempotent :: Polynomial Int Var2 -> Property
prop_expandIdempotent f = expand f === expand (expand f)


prop_substituteLinear a b
    = substitute (\X -> b :*: IEmpty) aTimesX `counterexampleEquiv` aTimesB where
        aTimesB :: Polynomial Int ()
        aTimesB = (a * b) :*: IEmpty :+: PEmpty
        aTimesX :: Polynomial Int Var1
        aTimesX = fromList1 [0, a]

prop_distributiveExpansion :: Int -> Property
prop_distributiveExpansion n = linear `power` 2 `counterexampleEquiv` quadratic where
    linear = fromList1 [n, 1]
    quadratic = fromList1 [n^2, 2 * n, 1]

prop_deleteUselessBrackets :: Property
prop_deleteUselessBrackets = once $ nonNested `counterexampleEquiv` nested where
    nonNested :: Polynomial Int Var1
    nonNested = pSimple . tSimple . iSimple . Var $ X
    nested :: Polynomial Int Var1
    nested = toPolynomial . Brackets $ nonNested

\end{code}

Ensure coefficients get translated nicely.
\begin{code}
prop_parseCoefficients :: Real -> Property
prop_parseCoefficients x = runParsePolynomial (show x) `counterexampleEquiv` fromList2 [[x]]
\end{code}

Check the parsing of exponents.
\begin{code}
prop_parseExponents :: Int -> Property
prop_parseExponents n = n > 0 ==>
    (runParsePolynomial ("x^"++show n++"y") `counterexampleEquiv` xToTheN) where
        factor `power` 0 = Var YY :.: IEmpty
        factor `power` n = factor :.: (factor `power` (n-1))
        xToTheN = 1 :*: (Var XX `power` n) :+: PEmpty
\end{code}

Also make sure sums work.
\begin{code}
prop_parseSums :: Real -> Real -> Property
prop_parseSums x y
    = runParsePolynomial (show x ++ " x + " ++ show y ++ " y") `counterexampleEquiv`
        fromList2 [[0, x], [y]]
\end{code}

The trigonometric polynomials shouldn't contain any products before we can apply the additors and multiplicators to calculate its value.
As long as the polynomial has no variables, only Sin and Cos factors, the |trigProdToSum| should eliminate all these.
\begin{code}
prop_trigProdToSumEliminatesTrigProds :: Polynomial Real TrigonometricVar2 -> Property
prop_trigProdToSumEliminatesTrigProds p
    = hasNoVars p ==> hasNoTrigProds . expand . trigProdToSum $ p where
        hasNoTrigProds
            = foldPolynomial ((const True, id, id, id), ((IEmpty, True), prod), const snd, (True, (&&)))
        f `prod` (i@(_ :.: _), _) = (Var Alpha :.: i, False)
        f `prod` (i, g) = (Var Alpha :.: i, f && g)
        hasNoVars :: Polynomial Real TrigonometricVar2 -> Bool
        hasNoVars
            = foldPolynomial ((const False, const True, const True, id), (True, (&&)), flip const, (True, (&&)))
\end{code}

\subsection{Single test cases}

We can also use the tests to verify a single example case that has been manually produced.
If the output of our implementation does not match this example case, we can be certain a mistake was made somewhere, but not if the mistake was made in the example or the program.

\begin{code}
-- x^2 - y
testPolynomial :: Polynomial Real Var2
testPolynomial
    = 1 :*: Var XX :.: Var XX :.: IEmpty :+:
    (-1) :*: Var YY :.: IEmpty :+:
    PEmpty

prop_testPolynomialFromList = once $
    testPolynomial `counterexampleEquiv` fromList2 [[0, 0, 1], [-1]]
prop_testPolynomialFromParse = once $
    testPolynomial `counterexampleEquiv` runParsePolynomial "x^2 - y"
testTranslatedPolynomial = translateXY testPolynomial (0, -4)

prop_translateWorks = once $
    testTranslatedPolynomial `counterexampleEquiv` fromList2 [[-4, 0, 1], [-1]]
testAsTrigonometry = substitute (pointToRadial 1 5) . expand $ testTranslatedPolynomial
-- Don't rely on `equiv` because the Cos / Sin terms should be adjacent!
prop_trigWorks = once $ expand testAsTrigonometry === expectedTrig where
    expectedTrig :: Polynomial Real TrigonometricVar2
    expectedTrig
        =
        1 :*: pcos (Var Alpha) :.: pcos (Var Alpha) :.: IEmpty :+:
        10 :*: pcos (Var Alpha) :.: pcos (Var Beta) :.: IEmpty :+:
        25 :*: pcos (Var Beta) :.: pcos (Var Beta) :.: IEmpty :+:
        (-4) :*: IEmpty :+:
        (-1) :*: psin (Var Alpha) :.: IEmpty :+:
        (-5) :*: psin (Var Beta) :.: IEmpty :+:
        PEmpty

testProdToSum = trigProdToSum . expand $ testAsTrigonometry

testCoefficients = simplifyTrigCoeff . expandedTrigCoeff . expand $ testProdToSum
prop_correctCoefficients = once $ testCoefficients === coeffs where
    coeffs :: Map.Map (TrigFunction2 Real) Real
    coeffs = Map.fromList
        [ (CosF 2 0, 0.5)
        , (CosF 0 0, 9)
        , (CosF 1 1, 5)
        , (CosF 1 (-1), 5)
        , (CosF 0 2, 12.5)
        , (SinF 1 0, -1)
        , (SinF 0 1, -5)
        ]

testFGammas = eliminateSines testCoefficients
prop_correctFGammas = once $ testFGammas === fGammas where
    fGammas :: Map.Map (Real, Real) (Real, Real)
    fGammas = Map.fromList
        [ ((0, 0), (9, 0))
        , ((2, 0), (0.5, 0))
        , ((1, 1), (5, 0))
        , ((1, -1), (5, 0))
        , ((0, 2), (12.5, 0))
        , ((1, 0), (1, pi/2))
        , ((0, 1), (5, pi/2))
        ]

testConfig = computeConfig testPolynomial ((0, 4), 1, 5) (1, 1)

testReversor = calcReversor (Reversor "O" "X" "Y" "Z" 1 1 1) (0, 0) (1, 0) (1 / sqrt 2, 1 / sqrt 2) (0, 1)

\end{code}

\subsection{Running the tests}

To run all tests, we use Template Haskell to collect all definitions and execute the tests whose name starts with |prop_|.

\begin{code}

return []
runTests = $quickCheckAll
\end{code}
