\section{Parsing} \label{lhs:Parser} \label{lhs:parser}

For easier input of polynomials, we can parse a more usual representation to the abstract data type.

\begin{code}

{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE Rank2Types #-}

module Parser where

import Prelude hiding (Real)

import Text.ParserCombinators.UU
import Text.ParserCombinators.UU.BasicInstances
import Text.ParserCombinators.UU.Utils

import Algebra
import Polynomial
import Reals
\end{code}

\subsection{Polynomial parameter parsers}

These parsers can be passed to the polynomial parser to define the field and variables of the polynomial.
These parsers are all lexeme parsers, skipping trailing whitespace.

\begin{code}
parseReal :: Parser Real
parseReal = lexeme pDoubleRaw

parseVar2 :: Parser Var2
parseVar2 = XX <$ pSymbol "x" <|> YY <$ pSymbol "y"
\end{code}

\subsection{Polynomial part parsing}

We parse the parts of a polynomial seperately.
Each part expects to be passed parsers for the field and variables of the polynomial, as defined in the previous section.
These parsers are all lexeme parsers, skipping trailing whitespace.
Their arguments should do so too.

A factor is either a variable or a polynomial surrounded by brackets.
\begin{code}
parseFactor :: (Ring k) => Parser k -> Parser v -> Parser (PFactor k v)
parseFactor parseField parseVar
    = Var <$> parseVar
    <|> Brackets <$ pSymbol "(" <*> parsePolynomial parseField parseVar <* pSymbol ")"
\end{code}

A number of indeterminates is just a product of factors.
We handle exponents here, since they are represented as separate factors in indeterminates.
To avoid ambiguity (e.g. "-3" can be an empty term minus a positive coefficient 3, or a term with a negative coefficient 3), we disallow empty indeterminates if the coefficient is emtpy too.
\begin{code}
parseIndeterminates :: (Ring k) => Parser k -> Parser v -> Bool -> Parser (PIndeterminates k v)
parseIndeterminates parseField parseVar canBeEmpty
    = (if canBeEmpty
      then pure IEmpty
      else (:.: IEmpty) <$> parseFactor parseField parseVar)
    <|> (:.:)
        <$> parseFactor parseField parseVar
        <*> parseIndeterminates parseField parseVar canBeEmpty
            -- we have to propagate canBeEmpty here to avoid ambiguity
            -- else, a single variable can be parsed in either one or two recursions
    <|> power
        <$> parseFactor parseField parseVar
        <* pSymbol "^"
        <*> pNatural
        <*> parseIndeterminates parseField parseVar True where
            factor `power` 0 = id
            factor `power` n
                | n > 0 = (factor :.:) . (factor `power` (n-1))
                | otherwise = error "Polynomials can't have negative degree!"
\end{code}

A term is a coefficient times a number of indeterminates.
If the coefficient is left out, we assume the multiplicative identity.
\begin{code}
parseTerm :: (Ring k) => Parser k -> Parser v -> Parser (PTerm k v)
parseTerm parseField parseVar
    = (identity :*:) <$> parseIndeterminates parseField parseVar False
    <|> (:*:) <$> parseField <*> parseIndeterminates parseField parseVar True
\end{code}

A polynomial, finally, consists of terms joined by '+' symbols.
A single term does not have to have a final '+' symbol.
You can also insert a - symbol to negate the following term only.
\begin{code}
parsePolynomial :: (Ring k) => Parser k -> Parser v -> Parser (Polynomial k v)
parsePolynomial parseField parseVar
    = (:+: PEmpty) <$> parseTerm parseField parseVar
    <|> (:+:)
        <$> parseTerm parseField parseVar
        <* pSymbol "+"
        <*> parsePolynomial parseField parseVar
    <|> (\t ((k :*: i) :+: p) -> t :+: (invert k :*: i) :+: p) -- negate only the following term
        <$> parseTerm parseField parseVar
        <* pSymbol "-"
        <*> parsePolynomial parseField parseVar
\end{code}

Finally, we can transform strings into polynomials.
Simply feed the string through the parser we constructed.
The |runParser| function produces an error if any corrections were made.
\begin{code}
runParsePolynomial :: String -> Polynomial Real Var2
runParsePolynomial = runParser "Polynomial" (parsePolynomial parseReal parseVar2)
\end{code}
