\section{Geometry} \label{lhs:geometry} \label{lhs:Geometry}

We need a number of functions for calculating intersections.

\begin{code}

module Geometry where

import Prelude hiding (Real)

import Algebra
import Reals
\end{code}

%format p_1
%format p_2
%format r_1
%format r_2
%format x_1
%format x_2
%format y_1
%format y_2

Intersect a circle with midpoint |p_1 = (x_1, y_1)| and radius |r_1| with a circle with midpoint |p_2 = (x_2, y_2)| and radius |r_2|.
Returns two intersection points, that might be the same point if the circles lie tangent.
If the circles coincide, returns infinity.
If circles do not intersect, returns infinity.
Based on \cite{Bourke}.

%format x_3
%format y_3
%format r_x
%format r_y
\begin{code}
circleIntersect :: Real2 -> Real -> Real2 -> Real -> (Real2, Real2)
circleIntersect p_1@(x_1, y_1) r_1 p_2@(x_2, y_2) r_2
    = ((x_3 - r_x, y_3 + r_y), (x_3 + r_x, y_3 - r_y)) where
        d = distance p_2 p_1
        a = (r_1^2 - r_2^2 + d^2) / (2 * d)
        x_3 = x_1 + a * (x_2 - x_1) / d
        y_3 = y_1 + a * (y_2 - y_1) / d
        h = sqrt(r_1^2 - a^2)
        r_x = h * (y_2 - y_1) / d
        r_y = h * (x_2 - x_1) / d
\end{code}

%format p_0

Intersect the line through |p_0| and |p_1| with the circle around |p_2| with radius |r|.
Returns two intersection points, which may be equal if the line and circle are tangent.
Returns infinity if they do not intersect.

%format x_0
%format y_0
%format lambda_1 = "\lambda_1"
%format lambda_2 = "\lambda_2"
%format d_x
%format d_y
\begin{code}
circleLineIntersect :: Real2 -> Real2 -> Real2 -> Real -> (Real2, Real2)
circleLineIntersect p_0@(x_0, y_0) p_1@(x_1, y_1) p_2@(x_2, y_2) r
    = (p_0 `add` (lambda_1 `leftMult` dv), p_0 `add` (lambda_2 `leftMult` dv)) where
        -- Substitute |p_0 `add` lambda `leftMult` (p_1 - p_0)| in the circle equation
        -- the direction vector of the line
        d_x = x_1 - x_0
        d_y = y_1 - y_0
        dv = (d_x, d_y)
        -- move the circle to the origin
        x_0' = x_0 - x_2
        y_0' = y_0 - y_2
        p_0' = (x_0', y_0')
        -- the parameters for our equation in |lambda|: $a \lambda^2 + b \lambda + c = 0$
        a = d_x^2 + d_y^2
        b = 2 * d_x * x_0' + 2 * d_y * y_0'
        c = x_0'^2 + y_0'^2 - r^2
        -- use the quadratic formula to solve the equation
        discriminant = sqrt (b^2 - 4 * a * c)
        lambda_1 = (-b + discriminant) / (2 * a)
        lambda_2 = (-b - discriminant) / (2 * a)

\end{code}
