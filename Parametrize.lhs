\section{Parametrize} \label{lhs:Parametrize}

To display graphics for user-input polynomials, we need to find their set of zeroes.
We define two ways to do this.
The first way is to make a function that precisely parametrizes this set.
The second way is to approximate the zeros using numerical methods, and linearly interpolating between the points found this way.

\begin{code}

module Parametrize where

import Prelude hiding (Real)

import Control.Monad
import Data.List
import qualified Data.Map as M
import Data.Tuple (swap)

import Algebra
import Polynomial
import Reals
import Util
\end{code}

An exact parametrization of a polynomial should have a function whose image over the reals is some (connected) subset of the zero set of the polynomial.
\begin{code}
data ParametrizedCurve = Parametrized (Polynomial Real Var2) (Real -> Real2)
\end{code}

We can approximate a curve with a list of points on that curve
Iterating over this list should approximate a path over the curve
In other words, a linear interpolation of these points should approximate a parametrization.
\begin{code}
data InterpolatedCurve = Interpolated (Polynomial Real Var2) [Real2]
\end{code}

\subsection{Interpolating parametrized curves}
We transform the special case of perfectly parametrized curves to the linearly interpolated ones.
This requires starting and ending points for the interpolation.
\begin{code}
interpolateCurve :: ParametrizedCurve -> Real -> Real -> InterpolatedCurve
interpolateCurve (Parametrized polynomial points)
    = Interpolated polynomial ... tracePoints points
\end{code}

Sample points from the interpolation.
\begin{code}
tracePoints :: (Real -> Real2) -> Real -> Real -> [Real2]
tracePoints points start end = points <$> interpolate start end where
\end{code}
The number of points on the curve is tweakable but $100$ is a good estimation.
\begin{code}
    resolution = 100
    interpolate start end = [start + (end - start) * t / resolution | t <- [0..resolution]]
\end{code}

A parametrization of a line segment between two points. Maps $0$ to the first point and $1$ to the second one.
\begin{code}
lerp :: Real2 -> Real2 -> Real -> Real2
lerp p_0 p_1 t = ((1 - t) `leftMult` p_0) `add` (t `leftMult` p_1)
\end{code}

Using linear interpolation, interpolate a sequence of $n$ points as a (non-looped) curve.
Passing a parameter less than 0 or greater than $n - 1$ causes an error.
\begin{code}
lerpCurve :: [Real2] -> Real -> Real2
lerpCurve points t = lerp p_t p_t' segmentDistance where
    pointIndex :: Int
    pointIndex = floor t
    segmentDistance :: Real
    segmentDistance = t - (fromIntegral pointIndex)
    p_t, p_t' :: Real2
    p_t = points !! pointIndex
    p_t' = points !! (pointIndex + 1)
\end{code}

\subsection{Interpolating implicit curves}

The principle of implicit interpolating is similar to the parametrized case. Take a number of samples on the curve, give them a reasonable ordering and use linear interpolation to get all of them.

\begin{code}
interpolateImplicit :: Real2 -> Real2 -> Polynomial Real Var2 -> InterpolatedCurve
interpolateImplicit (minX, minY) (maxX, maxY) polynomial
    = Interpolated polynomial points where
        resolution = 2000
        points = contours (minX, maxX, resolution) (minY, maxY, resolution) evaluate'
        evaluate' x y = evaluate (toEvaluate2 (x, y)) polynomial
\end{code}

Determine a contour line for a given function and a range.
This code is based on the |contours| function in Sacha Sokoloski's |Goal.Core.Plot.Contour| module \cite{goalcore}, which plots a number of contours for several given heights.
The modification plots a contour only for the height $0$.

The package \texttt{goal-core} is licensed under the BSD 3-Clause License, reproduced in the file LICENCE-goal-core.

\begin{code}
contours
    :: (Enum float, Floating float, Ord float)
    => (float,float,Int) -- ^ The range along the x axis.
    -> (float,float,Int) -- ^ The range along the y axis.
    -> (float -> float -> float) -- ^ The function to contour.
    -> [(float,float)] -- ^ (x, y)

-- Given various parameters and a function to analyze, contours returns a
-- list of points on the contour line with height 0
contours (xmn,xmx,nx) (ymn,ymx,ny) f =
    let lsts = functionToLists f (xmn,ymn) (xmx,ymx) stps
        stps = ((xmx - xmn) / fromIntegral nx,(ymx - ymn) / fromIntegral ny)
        (mn,mx) = (minimum $ minimum <$> lsts,maximum $ maximum <$> lsts)
     in contour lsts (xmx,ymx) stps 0

--- Internal ---


contour :: (Floating float, Ord float) => [[float]] -> (float,float) -> (float,float) -> float -> [(float,float)]
contour lsts mxs stps isolvl = linksToLines mxs stps =<< listsToLinks lsts isolvl

--- Contour Plot Internal ---


-- Types --

type Link = (Int,Int)
type ContourPair = (Link,Link)
data ContourBox =
    {- Styled like the indicies of a 3x3 Matrix. The lines are drawn from
       left to right, and if need be, top to bottom -}
    Empty
    | Line ContourPair
    | DLine ContourPair ContourPair
    deriving (Show,Eq)

data PairMap = PM (M.Map Link Link) (M.Map Link Link) deriving Show

-- Type Functions --

contourBoxesToPairMap :: [ContourBox] -> PairMap
contourBoxesToPairMap clns =
    let cprs = concatMap toPairs clns
    in PM (M.fromList cprs) (M.fromList $ map swap cprs)
    where toPairs (Line prpr) = [prpr]
          toPairs (DLine lprpr rprpr) = [lprpr,rprpr]

deletePair :: ContourPair -> PairMap -> PairMap
{- Deletes a left oriented line segmented -}
deletePair (lpr,rpr) (PM lmp rmp) =
    let lmp' = if M.lookup lpr lmp == Just rpr then M.delete lpr lmp else lmp
        rmp' = if M.lookup rpr rmp == Just lpr then M.delete rpr rmp else rmp
    in PM lmp' rmp'

popLink :: Link -> PairMap -> (Maybe Link,PairMap)
popLink lnk pmp@(PM lmp rmp) =
    let lft = M.lookup lnk lmp
        rgt = M.lookup lnk rmp
    in case (lft,rgt) of
           (Just rlnk,_) -> (Just rlnk,deletePair (lnk,rlnk) pmp)
           (_,Just llnk) -> (Just llnk,deletePair (llnk,lnk) pmp)
           _ -> (Nothing,pmp)

-- Core Algorithm --

functionToLists :: (Enum float, Floating float) => (float -> float -> float) -> (float,float) -> (float,float)
    -> (float,float) -> [[float]]
functionToLists f (xmn,ymn) (xmx,ymx) (xstp,ystp) =
    map (uncurry f) <$> [ [ (x,y) | x <- [xmx,(xmx - xstp)..xmn] ] | y <- [ymx,(ymx - ystp)..ymn] ]

situationTable :: (Floating float, Ord float) =>
    float -> (((Bool,float),(Bool,float)),((Bool,float),(Bool,float))) -> ContourBox
situationTable _ (((True,_),(True,_)),((True,_),(True,_))) = Empty
situationTable _ (((True,_),(True,_)),((False,_),(True,_))) = Line ((1,0),(2,1))
situationTable _ (((True,_),(True,_)),((True,_),(False,_))) = Line ((2,1),(1,2))
situationTable _ (((True,_),(True,_)),((False,_),(False,_))) = Line ((1,0),(1,2))
situationTable _ (((True,_),(False,_)),((True,_),(True,_))) = Line ((0,1),(1,2))
situationTable isolvl (((True,ul),(False,ur)),((False,ll),(True,lr)))
    | sum [ul,ur,ll,lr] / 4 < isolvl = DLine ((1,0),(0,1)) ((2,1),(1,2))
    | otherwise = DLine ((1,0),(2,1)) ((0,1),(1,2))
situationTable _ (((True,_),(False,_)),((True,_),(False,_))) = Line ((0,1),(2,1))
situationTable _ (((True,_),(False,_)),((False,_),(False,_))) = Line ((1,0),(0,1))
situationTable _ (((False,_),(True,_)),((True,_),(True,_))) = Line ((1,0),(0,1))
situationTable _ (((False,_),(True,_)),((False,_),(True,_))) = Line ((0,1),(2,1))
situationTable isolvl (((False,ul),(True,ur)),((True,ll),(False,lr)))
    | sum [ul,ur,ll,lr] / 4 < isolvl = DLine ((1,0),(2,1)) ((0,1),(1,2))
    | otherwise = DLine ((1,0),(0,1)) ((2,1),(1,2))
situationTable _ (((False,_),(True,_)),((False,_),(False,_))) = Line ((0,1),(1,2))
situationTable _ (((False,_),(False,_)),((True,_),(True,_))) = Line ((1,0),(1,2))
situationTable _ (((False,_),(False,_)),((False,_),(True,_))) = Line ((2,1),(1,2))
situationTable _ (((False,_),(False,_)),((True,_),(False,_))) = Line ((1,0),(2,1))
situationTable _ (((False,_),(False,_)),((False,_),(False,_))) = Empty

listsToContourBoxes :: (Floating float, Ord float) => [[float]] -> float -> [ContourBox]
listsToContourBoxes lsts isolvl = do
    (stnrw,r) <- zip stnlsts [0..]
    (stn,c) <- zip stnrw [0..]
    let bx = situationTable isolvl stn
    guard (bx /= Empty)
    return $ repositionBox (r,c) bx
    where stnlsts = rowZipper $ elementPairs . map threshold <$> lsts
          threshold x = (x >= isolvl,x)
          elementPairs lst = zip lst $ tail lst
          rowZipper rws = uncurry zip <$> elementPairs rws
          repositionContourPair (r,c) ((lr,lc),(rr,rc)) =
              ((lr + 2 * r, lc + 2 * c),(rr + 2 * r, rc + 2 * c))
          repositionBox rc (Line pr) =
              Line $ repositionContourPair rc pr
          repositionBox rc (DLine pr1 pr2) =
              DLine (repositionContourPair rc pr1) (repositionContourPair rc pr2)

traceLinks :: ContourPair -> PairMap -> ([Link],PairMap)
traceLinks (llnk,rlnk) pmp =
    let (llnks,pmp') = tracer [] (Just llnk,pmp)
        (rlnks,pmp'') = tracer [] (Just rlnk,pmp')
    in (llnks ++ reverse rlnks,pmp'')
    where tracer lnks (Just lnk,pmp') =
              tracer (lnk:lnks) $ popLink lnk pmp'
          tracer lnks (Nothing,pmp') = (lnks,pmp')

popLinks :: PairMap -> Maybe ([Link],PairMap)
popLinks pmp@(PM lmp _)
    | M.null lmp = Nothing
    | otherwise =
        let cpr = M.findMin lmp
        in Just (traceLinks cpr $ deletePair cpr pmp)

listsToLinks :: (Floating float, Ord float) => [[float]] -> float -> [[(Int,Int)]]
listsToLinks lsts isolvl =
    unfoldr popLinks .  contourBoxesToPairMap $ listsToContourBoxes lsts isolvl

linksToLines :: Floating float => (float,float) -> (float,float) -> [(Int,Int)] -> [(float,float)]
linksToLines (xmx,ymx) (xstp,ystp) lnks =
   (\(r,c) -> (xmx - fromIntegral c * xstp / 2,ymx - fromIntegral r * ystp / 2)) <$> lnks
\end{code}
