\section{Real numbers}

This module defines the real numbers $\R$.

\begin{code}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}

module Reals where

import Prelude hiding (Real)

import Algebra
\end{code}

%format Real = "\R "
%format Real2 = "\R^2 "
%format Real3 = "\R^3 "

The one, two and three-dimensional affine real spaces have straightforward definitions and instances.
\begin{code}
type Real = Float
type Real2 = (Real, Real)
type Real3 = (Real, Real, Real)

instance Monoid Real where
    mempty = 0
    mappend = (+)
instance Group Real where
    invert = negate
instance Ring Real where
    identity = 1
    multiply = (*)
    character = fromInteger
    power = (^)
instance Field Real where
    divide = (/)
\end{code}

%format lambda = "\lambda "
%format sqrt(x) = "\sqrt{" x "}"
%format a_1
%format a_2
%format b_1
%format b_2
\begin{code}
instance VectorSpace Real Real2 where
    lambda `leftMult` (a_1, a_2) = (lambda * a_1, lambda * a_2)
    vectorAdd (a_1, a_2) (b_1, b_2) = (a_1 + b_1, a_2 + b_2)
instance MetricSpace Real Real2 where
    distance (a_1, a_2) (b_1, b_2) = sqrt ((a_1 - b_1)^2 + (a_2 - b_2)^2)
instance NormSpace Real Real2 where
    norm (a_1, a_2) = sqrt (a_1^2 + a_2^2)
\end{code}

We also include two simple functions for basic geometric calculations.
\begin{code}
average :: VectorSpace Real v => v -> v -> v
average x y = (0.5 :: Real) `leftMult` (x `add` y)

normalize :: NormSpace Real v => v -> v
normalize x = (1 / norm x :: Real) `leftMult` x
\end{code}
