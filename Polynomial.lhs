\section{Polynomials} \label{lhs:Polynomial}

Here, we implement a system of polynomials with arbitrary variables over arbitrary fields.
These polynomials can additionally contain trigonometric terms |Cos| and |Sin|, unsupported in other algebra libraries.

\begin{code}
{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses #-}

module Polynomial where

import qualified Data.Map as Map
import Debug.Trace

import Algebra
\end{code}

%format X = "x"
%format XX = "x"
%format YY = "y"

While the variables in a polynomial can be arbitrary, in practice they are one of the following:
\begin{code}
data Var1 = X deriving (Eq, Show)
data Var2 = XX | YY deriving (Eq, Show)
\end{code}

Given a field |k| and a set of variables |v|, we define a polynomial over |k|.
There are quite a few alternative representations that didn't work out.
Lists of coefficients don't work nicely with sine and cosine and multiplication is hard to write succinctly.
Formal expressions with just a |(:+:)| and |:*:| operator don't reduce nicely to a canonical representation unless we define a lot of ordering operations.
Writing coefficients as a factor made it hard to find out the coefficient of a given set of indeterminates.
Starting indeterminates and polynomial with a single factor and term respectively, instead of an empty one, produces slightly more complicated logic in e.g. the |expand| function.
\begin{code}
data PFactor k v
    = Var v
    | Cos (Polynomial k v)
    | Sin (Polynomial k v)
    | Brackets (Polynomial k v)
    deriving Show

data PIndeterminates k v
    = IEmpty
    | PFactor k v :.: PIndeterminates k v
    deriving Show

data PTerm k v
    = k :*: PIndeterminates k v
    deriving Show

data Polynomial k v
    = PEmpty
    | PTerm k v :+: Polynomial k v
    deriving Show

infixr 6 :+:
infixr 7 :*:
infixr 8 :.:
\end{code}

The algebra of polynomials is a simple way to define catamorphisms.
Together with the |Polynomial k v| datatype, this forms an initial algebra, and defining a catamorphism is just a case of recursively applying all the functions in the algebra.

\begin{code}
type PolynomialAlgebra k v factor indeterminates term polynomial =
    ( (v -> factor, polynomial -> factor, polynomial -> factor, polynomial -> factor)
    , (indeterminates, factor -> indeterminates -> indeterminates)
    , k -> indeterminates -> term
    , (polynomial, term -> polynomial -> polynomial)
    )
foldPolynomial
    :: PolynomialAlgebra k v factor indeterminates term polynomial
    -> Polynomial k v
    -> polynomial
foldPolynomial ((var, cos, sin, brackets), (iempty, prod), coeff, (pempty, sum)) = foldP where
    foldP PEmpty = pempty
    foldP (t :+: p) = foldT t `sum` foldP p

    foldT (k :*: i) = k `coeff` foldI i

    foldI IEmpty = iempty
    foldI (f :.: i) = foldF f `prod` foldI i

    foldF (Var v) = var v
    foldF (Cos p) = cos . foldP $ p
    foldF (Sin p) = sin . foldP $ p
    foldF (Brackets f) = brackets . foldP $ f
\end{code}

\subsection{Part manipulation}

Polynomials form a recursive data type with promotions of all subtypes. To make this process easier, we define utility functions to convert arbitrary polynomial parts to arbitrary others by appending the empty constructors.

The |[itp]Simple| functions perform a single upward promotion.
\begin{code}
iSimple :: PFactor k v -> PIndeterminates k v
iSimple = (:.: IEmpty)
tSimple :: Ring k => PIndeterminates k v -> PTerm k v
tSimple = (identity :*:)
pSimple :: PTerm k v -> Polynomial k v
pSimple = (:+: PEmpty)
\end{code}

The functions in this typeclass are recursively defined. If you implement a |from| and a |to| function, the other functions will automatically work. As always, overriding various implementations may be more efficients.
\begin{code}
class PolynomialPart p where
    fromFactor :: (Ring k) => PFactor k v -> p k v
    fromFactor = fromIndeterminates . toIndeterminates
    toFactor :: (Ring k) => p k v -> PFactor k v
    toFactor = Brackets . toPolynomial
    fromIndeterminates :: (Ring k) => PIndeterminates k v -> p k v
    fromIndeterminates = fromTerm . toTerm
    toIndeterminates :: (Ring k) => p k v -> PIndeterminates k v
    toIndeterminates = iSimple . toFactor
    fromTerm :: (Ring k) => PTerm k v -> p k v
    fromTerm = fromPolynomial . toPolynomial
    toTerm :: (Ring k) => p k v -> PTerm k v
    toTerm = tSimple . toIndeterminates
    fromPolynomial :: (Ring k) => Polynomial k v -> p k v
    fromPolynomial = fromFactor . toFactor
    toPolynomial :: (Ring k) => p k v -> Polynomial k v
    toPolynomial = pSimple . toTerm
\end{code}

Substitution can also produce arbitrary parts, for example when replacing variables with cosines, or replacing variables with full polynomials.
\begin{code}
    substitute :: (Ring k) => (v -> p k w) -> Polynomial k v -> Polynomial k w
    substitute vars = foldPolynomial
        ( (toFactor . vars, Cos, Sin, Brackets)
        , (IEmpty, (:.:))
        , (:*:)
        , (PEmpty, (:+:))
        )

instance PolynomialPart PFactor where
    fromFactor = id
    toFactor = id
instance PolynomialPart PIndeterminates where
    fromIndeterminates = id
    toIndeterminates = id
instance PolynomialPart PTerm where
    fromTerm = id
    toTerm = id
instance PolynomialPart Polynomial where
    fromPolynomial = id
    toPolynomial = id
\end{code}

\subsection{Typeclass instances}

We implement basic typeclasses for the |Polynomial k v| type.
When it is useful, we also define it for any other parts.

\begin{code}
instance (Ring k, Eq k, Eq v) => Eq (PFactor k v) where
    Var x == Var y = x == y
    (Cos p) == (Cos q) = p == q -- We don't check for equivalence, only literal equality
    (Sin p) == (Sin q) = p == q
    Brackets p == Brackets q = p == q
    _ == _ = False
instance (Ring k, Eq k, Eq v) => Eq (PIndeterminates k v) where
    IEmpty == IEmpty = True
    (f :.: IEmpty) == (f' :.: IEmpty) = f == f'
    (f :.: (g :.: h)) == (f' :.: (g' :.: h'))
        = (f == f' && g :.: h == g' :.: h')
        || (f == g' && g :.: h == f' :.: h')
    _ == _ = False
instance (Ring k, Eq k, Eq v) => Eq (PTerm k v) where
    (k :*: f) == (k' :*: f') = k == k' && f == f'
instance (Ring k, Eq k, Eq v) => Eq (Polynomial k v) where
    PEmpty == PEmpty = True
    (f :+: PEmpty) == (f' :+: PEmpty) = f == f'
    (f :+: (g :+: h)) == (f' :+: (g' :+: h'))
        = (f == f' && g :+: h == g' :+: h')
        || (f == g' && g :+: h == f' :+: h')
    _ == _ = False

instance (Ring k) => Monoid (Polynomial k v) where
    mempty = PEmpty
    mappend = pconcat
instance (Ring k) => VectorSpace k (Polynomial k v) where
    leftMult k f = toPolynomial $ k :*: toIndeterminates f
    vectorAdd = add
instance (Ring k) => Group (Polynomial k v) where
    invert f = toPolynomial $ invert identity :*: toIndeterminates f
instance (Ring k) => Ring (Polynomial k v) where
    identity = toPolynomial $ identity :*: IEmpty
    multiply f g = toPolynomial $ toFactor f :.: toIndeterminates g
\end{code}

\subsection{Manipulation of polynomials}

Similar to the |(++)| function, we can concatenate products and sums for easier manipulation.
\begin{code}
iconcat :: PIndeterminates k v -> PIndeterminates k v -> PIndeterminates k v
iconcat IEmpty fs = fs
iconcat (f :.: fs) gs = f :.: iconcat fs gs
pconcat :: Polynomial k v -> Polynomial k v -> Polynomial k v
pconcat PEmpty fs = fs
pconcat (f :+: fs) gs = f :+: pconcat fs gs
\end{code}

An important class of polynomials is those without |Brackets|, as they do not have a recursive structure.
By repeatedly using the distributive properties, we can convert a polynomial with brackets into an equivalent polynomial without them.
This does \emph{not} produce a canonical form. For example, variables are not reordered.
\begin{code}
expand :: (Eq k, Eq v, Ring k) => Polynomial k v -> Polynomial k v
expand = foldPolynomial ((var, cos, sin, brackets), (iempty, prod), coeff, (pempty, sum)) where
    infixr 6 `sum`
    infixr 7 `coeff`
    infixr 8 `prod`

    icat IEmpty fs = fs
    icat (f :.: fs) gs = f `prod` icat fs gs

    sum :: (Eq k, Eq v, Ring k) => PTerm k v -> Polynomial k v -> Polynomial k v
    var = Var
    cos = Cos
    sin = Sin
    -- delete useless brackets
    brackets (k :*: f :.: IEmpty :+: PEmpty) | k == identity = f
    brackets f = Brackets f

    iempty = IEmpty
    -- delete useless brackets
    Brackets (k :*: f :+: PEmpty) `prod` IEmpty | k == identity = f
    -- distributivity
    Brackets ((k :*: f) :+: g) `prod` (i :.: j)
        = Brackets (
            k `coeff` i `prod` f `sum`
            identity `coeff` i `prod` brackets g `prod` IEmpty `sum`
            PEmpty
          ) `prod` j
    -- move coefficients to the front, so they get absorbed later on
    -- keep this one low down because it catches most cases
    Brackets f `prod` g  = brackets f :.: g
    f `prod` (Brackets h :.: g) = brackets h `prod` f `prod` g
    -- default case
    f `prod` g = f :.: g

    -- delete useless brackets
    k `coeff` (Brackets (f :+: PEmpty) :.: IEmpty) | k == identity = f
    -- get rid of identity elements
    k `coeff` (Brackets PEmpty :.: f) = -- trace "coeff pempty" $
        mempty `coeff` f
    -- absorb any lower coefficients
    k `coeff` (Brackets (k' :*: f :+: PEmpty) :.: g) =
        k `multiply` k' `coeff` (f `icat` g)
    -- default case
    k `coeff` f = -- trace "coeff default" $
        k :*: f

    pempty = PEmpty
    -- delete useless brackets
    (k :*: Brackets f :.: IEmpty) `sum` PEmpty | k == mempty = f
    -- get rid of identity elements
    (k :*: _) `sum` f | k == mempty = f
    -- add equal terms
    (k :*: f) `sum` (k' :*: f' :+: g) | f == f' = k `add` k' `coeff` f `sum` g
    -- distributivity
    (k :*: Brackets (k' :*: f :+: g) :.: h) `sum` i =
        k `multiply` k' `coeff` (f `iconcat` h) `sum`
        k `coeff` brackets g `prod` h `sum`
        i
    -- default case
    f `sum` g = f :+: g
\end{code}

Usually, polynomials are assumed to have no trigonometric expressions in their factors, only variables.
The |hasTrig| function checks that a polynomial only has variables and brackets in its factors.
\begin{code}
hasTrig :: Polynomial k v -> Bool
hasTrig = foldPolynomial
    ( (const False, const True, const True, id)
    , (False, (||))
    , flip const
    , (False, (||))
    )
\end{code}

Often, a substitution produces many brackets. By expanding both polynomials, we define a better concept of them being ``the same'' polynomial.
The |(==)| operator simply checks their syntactic equality.
\begin{code}
equiv :: (Ring k, Eq k, Eq v) => Polynomial k v -> Polynomial k v -> Bool
p `equiv` q = expand p == expand q
\end{code}

Evaluate a polynomial as if it were a function with its variables as parameters.
The parameter |f :: v -> k| maps the variables to a value in the ring of the polynomial, and can be easily constructed using a |toEvaluate| function below.
We require |k| to be a type of floating point numbers so we can evaluate the |Cos| and |Sin| factors.
\begin{code}
evaluate :: (Ring k, Floating k) => (v -> k) -> Polynomial k v -> k
evaluate f = foldPolynomial ((f, cos, sin, id), (identity, multiply), multiply, (mempty, add))
\end{code}

For |Var1| and |Var2|, writing the mapping function every time you want to evaluate is quite cumbersome.
These utility functions do that for you.
\begin{code}
toEvaluate1 :: a -> Var1 -> a
toEvaluate1 = const

toEvaluate2 :: (a, a) -> Var2 -> a
toEvaluate2 (x, _) XX = x
toEvaluate2 (_, y) YY = y
\end{code}

Translate a polynomial in two variables along a vector |(a, b)|, substituting variables |XX| and |YY| with |XX - a|, |YY - b|.
\begin{code}
-- replace a and b with (a - x, b - y)
translateXY :: Ring k => Polynomial k Var2 -> (k, k) -> Polynomial k Var2
translateXY f (x, y) = substitute translation f
    where translation XX = invert x :*: IEmpty :+: (toPolynomial . Var $ XX)
          translation YY = invert y :*: IEmpty :+: (toPolynomial . Var $ YY)
\end{code}

\subsection{Representing and constructing polynomials}
To map polynomials to a reasonably human-readable form, we give functions to pretty-print them.
Additionally, to construct polynomials from a reasonably human-readable form, we give functions to construct them from lists.
The module |Parser| (\ref{lhs:parser}) contains a parser that can convert strings of a more usual notation into polynomials.

\begin{code}
ppPolynomial :: (Show k, Show v) => Polynomial k v -> String
ppPolynomial = foldPolynomial ((var, cos, sin, brackets), (iempty, prod), coeff, (pempty, sum)) where
    var :: (Show v) => v -> String
    var = show
    cos = ("cos (" ++) . (++ ")")
    sin = ("sin (" ++) . (++ ")")
    brackets = ("(" ++) . (++ ")")
    iempty = "1"
    prod f "1" = f
    prod f g = f ++ " " ++ g
    coeff k f = show k ++ " " ++ f
    pempty = "0"
    sum f "0" = f
    sum f g = f ++ " + " ++ g

-- X -> [1, 2, 3] -> X + 2X^2 + 3X^3
fromList' :: v -> PIndeterminates k v -> [k] -> Polynomial k v
fromList' _ _ [] = PEmpty
fromList' _ i [k] = k :*: i :+: PEmpty
fromList' v i (k:ks) = k :*: i :+: fromList' v (Var v :.: i) ks

-- [1, 2, 3] -> 1 + 2x + 3x^2
fromList1 :: Ring k => [k] -> Polynomial k Var1
fromList1 = fromList' X IEmpty

-- [[1, 2], [3, 4] -> 1 + 2x + 3y + 4xy
fromList2 :: Ring k => [[k]] -> Polynomial k Var2
fromList2 = fromList'' XX YY IEmpty where
    fromList'' _ _ _ [] = PEmpty
    fromList'' v _ i [ks] = fromList' v i ks
    fromList'' v v' i (ks:kss) = fromList' v i ks `pconcat` fromList'' v v' (Var v' :.: i) kss

\end{code}
