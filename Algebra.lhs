\section{Algebra}

This section defines a number of typeclasses for concepts in abstract algebra, as Haskell's built in |Num| typeclass is too general for our purposes.
Except for the numeric implementations, we try to use the most general type to describe our algorithms.
This allows our code to be used in other contexts requiring simple algebra transformations.

\begin{code}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}

module Algebra where

import Prelude hiding (subtract)
\end{code}

\subsection{Groups}
%format `add` = "\mathbf{+}"
%format zero = "\mathbf{0}"
%format invert = "\mathbf{-}"
A group with binary operator |`add`| (also denoted as |add|), identity element |zero| and unary operator |invert| satisfying the group laws:
\begin{itemize}
    \item |`add`| is associative, |g `add` (g' `add` g'') == (g `add` g') `add` g''| (inherited from |Monoid|)
    \item |zero| is the identity element (inherited from |Monoid|)
    \item |invert| gives the inverse of an element, such that |g `add` invert g == invert g `add` g == zero|
\end{itemize}
We use additive language to describe groups so we can use the same methods for the |Ring| typeclass.
\begin{code}
class (Monoid g) => Group g where
    invert :: g -> g
    add :: g -> g -> g
    add = mappend
    zero :: g
    zero = mempty
\end{code}
%format `subtract` = "\mathbf{-}"
We define a |subtract| method for utility and compatibility with |Num|.
Subtracting does the same as adding the inverse, i.e. |g `subtract` g' == g `add` invert g'|.
\begin{code}
    subtract :: g -> g -> g
    subtract g h = g `add` invert h
\end{code}

The cartesian product of two |Group|s has an easy to define structure.
\begin{code}
instance (Group a, Group b) => Group (a, b) where
    invert ((a, b)) = (invert a, invert b)
\end{code}

\subsection{Rings}
%format `multiply` = "\mathbf{\cdot}"
%format identity = "\mathbf{1}"
%format `power` = "\mathbf{\uparrow}"
We define a ring as an abelian group together with an associative operator |`multiply`| (also denoted |multiply|) with identity element |identity|.
A |Ring| is not necessarily commutative, but must have an identity element.
Satisfies the following laws:
\begin{itemize}
    \item |`add`| is associative, |g `add` (g' `add` g'') == (g `add` g') `add` g''| (inherited from |Group|)
    \item |zero| is the identity element for |`add`| (inherited from |Group|)
    \item |invert| gives the inverse of an element, such that |g `add` invert g == invert g `add` g == zero| (inherited from |Group|)
    \item |`add`| is commutative
    \item |`multiply`| is associative
    \item |identity| is the identity element for |`multiply`| on all elements except |zero|
\end{itemize}
The last laws together give a |Monoid| instance for the ring without |zero|.
\begin{code}
class (Group a) => Ring a where
    identity :: a
    multiply :: a -> a -> a
\end{code}

There is a natural homomorphism of the integers into the |Ring|.
The default implementation has a complexity of $\order{\abs{n}}$ where $n$ is the argument to |character|.
|Ring|s that naturally embed the integers should probably override this implementation.
\begin{code}
    character :: Integer -> a
    character n | n > 0  = identity `add` character (n-1)
                | n < 0  = invert (character (negate n))
                | n == 0 = zero
\end{code}

Raise the ring element to the power of $n \in \N$ by multiplying it with itself $n$ times.
The result for negative $n$ is undefined (when not in a |Field|).
\begin{code}
    power :: a -> Int -> a
    power k n = iterate (multiply k) identity !! n
\end{code}

Similarly, the cartesian product of two |Ring|s has an easy to define structure.
\begin{code}
instance (Ring a, Ring b) => Ring (a, b) where
    identity = (identity, identity)
    multiply (a, b) (c, d) = (a `multiply` c, b `multiply` d)
\end{code}

\subsection{Fields}
%format `divide` = "\mathbf{/}"

A commutative ring together with an inverted operator to multiplying.
Usually, this is defined in terms of a unary inverse function.
For compatibility with the basic math operators, we use a binary division operator |`divide`| (also denoted |divide|).

Satisfies the following laws:
\begin{itemize}
    \item |`add`| is associative, |g `add` (g' `add` g'') == (g `add` g') `add` g''| (inherited from |Group|)
    \item |zero| is the identity element for |`add`| (inherited from |Group|)
    \item |invert| gives the inverse of an element, such that |g `add` invert g == invert g `add` g == zero| (inherited from |Group|)
    \item |`add`| is commutative
    \item |`multiply`| is associative
    \item |identity| is the identity element for |`multiply`| on all elements except |zero|
    \item |`multiply`| is commutative
    \item |`divide`| is the inverse operator for |`multiply`| on all elements except |zero|
    \item Extend the definition of |power| such that |k `power` n = identity `divide` (k `power` invert n)|.
\end{itemize}
The last laws together give a |Group| instance for the ring without |zero|.

\begin{code}
class (Ring k) => Field k where
    divide :: k -> k -> k
\end{code}

\subsection{Spaces}
Finally, we define basic spaces to perform linear algebra on.

%format `leftMult` = "\mathbf{\cdot}"
%format `vectorAdd` = "\mathbf{+}"

A vector space over a |Ring| of scalars is an abelian group with an operator that distributes over addition and multiplication in the |Ring|.
\begin{code}
class (Ring k, Group v) => VectorSpace k v where
    leftMult :: k -> v -> v
    vectorAdd :: v -> v -> v
\end{code}

A ring immediately functions as a vector space over itself.
\begin{code}
instance (Ring k) => VectorSpace k k where
    leftMult = multiply
    vectorAdd = add
\end{code}

These two spaces have a concept of length.
\begin{code}
class (Field k, Ord k) => MetricSpace k v where
    distance :: v -> v -> k
\end{code}

%format norm(v) = "\mathbf{\|}" v "\mathbf{\|}"
\begin{code}
class (Field k, VectorSpace k v, MetricSpace k v) => NormSpace k v where
    norm :: v -> k
\end{code}
